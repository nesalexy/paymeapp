FROM python:3

MAINTAINER Alex

RUN apt-get update

# Install wkhtmltopdf
RUN curl -L#o wk.tar.xz https://downloads.wkhtmltopdf.org/0.12/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz \
    && tar xf wk.tar.xz \
    && cp wkhtmltox/bin/wkhtmltopdf /usr/bin \
    && cp wkhtmltox/bin/wkhtmltoimage /usr/bin \
    && rm wk.tar.xz \
    && rm -r wkhtmltox

# for celery
ENV APP_USER user
ENV APP_ROOT /src

RUN groupadd -r ${APP_USER} \
    && useradd -r -m \
    --home-dir ${APP_ROOT} \
    -s /usr/sbin/nologin \
    -g ${APP_USER} ${APP_USER}

# create directory for application source code
RUN mkdir -p /usr/django/app

COPY requirements.txt /usr/django/app/

WORKDIR /usr/django/app

RUN pip install -r requirements.txt
RUN pip install --upgrade https://github.com/celery/celery/tarball/master