import uuid

from django.contrib.auth.models import User
from django.db import models
from .constans import CN_NAME


class SocialAccounts(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    sn_name = models.CharField(choices=CN_NAME, max_length=20, blank=False)
    sn_id = models.CharField(blank=False, max_length=50, unique=True)
    user = models.ForeignKey(User, related_name='sn_accounts', on_delete=models.CASCADE)
    access_token = models.CharField(blank=False, max_length=220, unique=True)
    refresh_token = models.CharField(blank=False, max_length=220, unique=True)

    is_active = models.BooleanField(default=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True, blank=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True)

    class Meta:
        db_table = "social_accounts"
        unique_together = (("sn_name", "sn_id", "user"),)
