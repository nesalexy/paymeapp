from django.contrib.auth.models import User
from rest_framework import generics, permissions, status

from accounts.models import SocialAccounts
from accounts.serializers import SNAuthTokenSerializer
from rest_framework.response import Response

from rest_framework.authtoken.models import Token


class SNAuthToken(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = SNAuthTokenSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        if serializer.validated_data['sn_name'] == "google":
            return self.__google_plus_auth(serializer)

    def __google_plus_auth(self, serializer):
        sn_profile = serializer.validated_data['sn_profile']
        access_token = serializer.validated_data['access_token']
        refresh_token = serializer.validated_data['refresh_token']

        try:
            # SN account exist, just sing in user
            sa_object = SocialAccounts.objects.get(sn_name=serializer.validated_data['sn_name'],
                                                   sn_id=sn_profile['id'])
            user = sa_object.user
            return self.create_token(user)
        except SocialAccounts.DoesNotExist:
            try:
                # SN account create
                user = User.objects.get(email=sn_profile['email'])
                user.is_active = True
                user.save()
                SocialAccounts.objects.create(sn_id=sn_profile['id'],
                                              sn_name=serializer.validated_data['sn_name'],
                                              user=user,
                                              access_token=access_token,
                                              refresh_token=refresh_token)
                return self.create_token(user)
            except User.DoesNotExist:
                # SN account and user create
                random_password = User.objects.make_random_password(6)
                user = User.objects.create_user(username='google_' + sn_profile['email'].split("@")[0],
                                                email=sn_profile['email'],
                                                password=random_password,
                                                first_name=sn_profile['first_name'],
                                                last_name=sn_profile['last_name'],
                                                is_active=True)

                # if sn_profile['avatar'] is not None:
                #     user.avatar.save("{}.jpg".format(user.id), get_image_from_url(sn_profile['avatar']),
                #                      save=True)
                SocialAccounts.objects.create(sn_id=sn_profile['id'],
                                              sn_name=serializer.validated_data['sn_name'],
                                              user=user,
                                              access_token=access_token,
                                              refresh_token=refresh_token)
                return self.create_token(user)

    def create_token(self, user):
        token, created = Token.objects.get_or_create(user=user)
        token_key = token.key

        return Response({'token': token_key, 'user': user.id}, status.HTTP_201_CREATED)
