import httplib2
from rest_framework import exceptions
from googleapiclient.discovery import build
from rest_framework import serializers
from oauth2client.client import AccessTokenCredentials, AccessTokenCredentialsError

from accounts.constans import CN_NAME
from imap_gmail.imap_connect import ImapMailConnect
from imap_gmail.imap_helper import generateOAuth2String


class SNAuthTokenSerializer(serializers.Serializer):
    sn_name = serializers.ChoiceField(choices=CN_NAME, label="Social network name")
    access_token = serializers.CharField(label="Social access token")
    refresh_token = serializers.CharField(label="Social refresh token")

    def get_google_plus_profile(self, access_token):
        try:
            credentials = AccessTokenCredentials(
                access_token=access_token,
                user_agent='my-user-agent/1.0')
            http_auth = credentials.authorize(httplib2.Http())
            plus = build('plus', 'v1', http=http_auth)
            people_resource = plus.people()
            return people_resource.get(userId='me').execute()
        except AccessTokenCredentialsError:
            msg = 'Invalid token'
            raise exceptions.ParseError(msg)

    def validate(self, attrs):
        sn_name = attrs.get('sn_name')
        access_token = attrs.get('access_token')
        refresh_token = attrs.get('refresh_token')

        if sn_name and access_token and refresh_token:
            sn_profile_valid = {}

            if sn_name == "google":
                sn_profile = self.get_google_plus_profile(access_token)
                sn_profile_valid['id'] = sn_profile.get('id')
                sn_profile_valid['email'] = sn_profile['emails'][0].get('value')
                sn_profile_valid['first_name'] = sn_profile.get('name').get('givenName')
                sn_profile_valid['last_name'] = sn_profile.get('name').get('familyName')
                if sn_profile_valid['email'] is None:
                    msg = 'Permission must include "email" in profile.'
                    raise exceptions.ParseError(msg)

                self.check_permission_email(sn_profile_valid['email'], access_token)

        else:
            msg = 'Must include "access_token" and "sn_name" and "refresh_token" '
            raise exceptions.ParseError(msg)

        attrs['sn_profile'] = sn_profile_valid
        # attrs['custom_role'] = get_user_role(user)
        return attrs

    def check_permission_email(self, user_email, access_token):
        try:
            oAuth2String = generateOAuth2String(user_email, access_token)
            self.imap = ImapMailConnect(oAuth2String)
        except BaseException as e:
            msg = str(e)
            raise exceptions.ParseError(msg)

        if oAuth2String:
            is_authenticate = self.imap.__enter__()
            if is_authenticate:
                self.imap.__exit__()
            else:
                msg = 'Permission must include "imap_gmail scope(s)".'
                raise exceptions.ParseError(msg)