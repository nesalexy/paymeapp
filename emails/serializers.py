from rest_framework import serializers

from emails.models import Emails, Files


class FilesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Files
        fields = ('id',
                  'name',
                  'url'
                  )


class EmailsSerializer(serializers.ModelSerializer):
    files_list = FilesSerializer(many=True, read_only=True)

    class Meta:
        model = Emails
        fields = ('id',
                  'user',
                  'mail_date',
                  'mail_id',
                  'mail_theme',
                  'mail_from',
                  'is_paid',
                  'new_item',
                  'created_at',
                  'files_list',
                  )
        read_only_fields = (
                          'is_paid',
                          'mail_id',
                          'new_item',
                          )


class EmailsDetailSerializer(serializers.ModelSerializer):
    files_list = FilesSerializer(many=True, read_only=True)

    class Meta:
        model = Emails
        fields = ('id',
                  'user',
                  'mail_date',
                  'mail_id',
                  'mail_theme',
                  'mail_from',
                  'is_paid',
                  'new_item',
                  'created_at',
                  'files_list',
                  )

        read_only_fields = ('id',
                  'user',
                  'mail_date',
                  'new_item',
                  'mail_id',
                  )
