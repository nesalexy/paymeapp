from django.urls import path, re_path

from emails import views

urlpatterns = [

    path('', views.EmailsView.as_view()),
    re_path('(?P<pk>[0-9A-Za-z_\-]+)/$', views.EmailDetailView.as_view()),

]

