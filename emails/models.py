from datetime import datetime

from django.contrib.auth.models import User
from django.db import models


class Emails(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_found_letters')
    mail_date = models.DateTimeField(auto_now_add=False, default=datetime.now, blank=True)
    mail_theme = models.CharField(max_length=254, blank=True)
    mail_id = models.CharField(max_length=512, blank=False)
    mail_from = models.CharField(max_length=126, blank=True)
    is_paid = models.BooleanField(default=False)
    new_item = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_at = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return "%s" % (self.mail_from)


class Files(models.Model):
    found_letter = models.ForeignKey(Emails, on_delete=models.CASCADE, related_name='files_list')
    name = models.CharField(max_length=128, blank=False)
    url = models.CharField(max_length=256, blank=False)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_at = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return "%s" % (self.name)