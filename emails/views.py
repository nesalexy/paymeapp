import os

from django_filters.rest_framework import DjangoFilterBackend
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics, permissions, exceptions, status
from rest_framework.decorators import api_view

from emails.models import Emails, Files
from emails.serializers import EmailsSerializer, EmailsDetailSerializer
from rest_framework.response import Response

from imap_gmail.constans import FILE_PATH


class EmailsView(generics.ListCreateAPIView):
    """
    Senders that will be searched\n
    `GET` - return all founded emails
    `POST` - create new item
    `Filter` - by fields `is_paid`, `new_item`, `mail_from`
    """
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = EmailsSerializer
    filter_backends = (DjangoFilterBackend, )
    filter_fields = ('is_paid', 'new_item', 'mail_from')

    def get_queryset(self, *args, **kwargs):
        return Emails.objects.all().filter(user=self.request.user)


class EmailDetailView(generics.RetrieveUpdateDestroyAPIView):
    """

    `{id}` - id your mail from mails list
    `GET` - return detail description by item
    `PUT` - you can change `mail_theme`, `mail_from`, `is_paid`
    `DELETE` - also you can delete some item
    """
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = EmailsDetailSerializer
    queryset = Emails.objects.all()

    def get_object(self):
        try:
            found_letter = Emails.objects.get(user=self.request.user, id=self.kwargs['pk'])
            if found_letter.new_item:
                found_letter.new_item = False
                found_letter.save()
            return found_letter
        except Emails.DoesNotExist:
            msg = 'Not found'
            raise exceptions.NotFound(msg)
