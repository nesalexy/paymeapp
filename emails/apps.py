from django.apps import AppConfig


class EmailConf(AppConfig):
    name = 'emails'
