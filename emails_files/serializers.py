from rest_framework import serializers

from emails.models import Files


class FilesDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Files
        fields = ('id',
                  'name',
                  'url',
                  )

        read_only_fields = ('id',
                            'url',
                            )