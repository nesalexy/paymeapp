from django.apps import AppConfig


class EmailsFilesConfig(AppConfig):
    name = 'emails_files'
