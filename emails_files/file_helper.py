import email


def TNEF(param):
    pass


def additional_check_for_dat_files(part):
    new_attachments = []
    try:
        filename_orig = str(part.get_filename())
        if filename_orig and filename_orig.strip().lower() in ['winmail.dat', 'win.dat']:
            try:
                winmail = TNEF(part.get_payload(decode=True))
                for attach in winmail.attachments:
                    new_attachments.append((get_name_of_file(attach.name.decode("utf-8")), attach.data))
            except BaseException as e:
                raise Exception

    except BaseException as e:
        print(e)

    return new_attachments


def get_name_of_file(name_file):
    try:
        decode_filename = decode_mime_words(name_file.encode("ascii", errors="ignore").decode())
    except BaseException as e:
        decode_filename = None

    return decode_filename


def decode_mime_words(s):
    return u''.join(
        word.decode(encoding or 'utf8') if isinstance(word, bytes) else word
        for word, encoding in email.header.decode_header(s))