from django.urls import path, re_path

from emails_files import views

urlpatterns = [
    path('<str:user_id>/<str:file_name>/', views.view_file),
    re_path('(?P<pk_email>[0-9A-Za-z_\-]+)/files/(?P<pk_file>[0-9A-Za-z_\-]+)/$', views.EmailDetailFiles.as_view()),
]

