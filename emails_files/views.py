from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics, permissions, exceptions
from rest_framework.decorators import api_view

from emails.models import Files, Emails
from emails_files.serializers import FilesDetailSerializer
from imap_gmail.constans import FILE_PATH
import mimetypes

@api_view(['GET', ])
@csrf_exempt
def view_file(request, user_id, file_name):
    """
    `/<user_id>/<file_name>/` - this inf you can get when you view detail inf mail, this is filed `url`.
    `GET` - return detail description by item.
    """

    if request.method == 'GET':
        try:
            full_path_file = FILE_PATH + '/' + str(user_id) + '/' + file_name
            # mimetype = magic.from_file(full_path_file, mime=True)
            mimetype = mimetypes.MimeTypes().guess_type(full_path_file)[0]

            with open(full_path_file, 'rb') as pdf:
                response = HttpResponse(pdf.read(), content_type=mimetype)
                return response

        except BaseException as e:
            return JsonResponse({'status': 'false', 'message': 'file not found'}, status=404)


class EmailDetailFiles(generics.RetrieveUpdateDestroyAPIView):
    """
    `/<pk_email>/files/<pk_file>/` - `<pk_email>` id from mails list, `<pk_file>` id file.
    `GET` - return detail description by item.
    `PUT` - you can change `name` of file.
    `DELETE` - also you can delete some item
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = FilesDetailSerializer
    queryset = Files.objects.all()

    def get_object(self):
        try:
            try:
                user_email = Emails.objects.get(pk=self.kwargs['pk_email'])
            except Emails.DoesNotExist:
                msg = 'This email not found'
                raise exceptions.NotFound(msg)

            return Files.objects.get(found_letter=user_email, pk=self.kwargs['pk_file'])

        except Files.DoesNotExist:
            msg = 'This file not found'
            raise exceptions.NotFound(msg)
