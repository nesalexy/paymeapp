import ntpath
import os
import shutil
from tempfile import NamedTemporaryFile

from emails.models import Files
from imap_gmail.constans import FILE_PATH, PDF_FILE_NAME


class File_Worker:

    def __init__(self, user_id):
        self.path_file = FILE_PATH
        self.user_id = user_id
        self.full_path_file = FILE_PATH + '/' + str(self.user_id)

    def save_payload_to_folder(self, filename, payload, db_found_letter):
        self.__check_or_create_folder()
        extension = os.path.splitext(filename)[1]

        with NamedTemporaryFile(delete=False, dir=self.full_path_file, suffix=extension) as f:
            f.write(payload)
            f.flush()
            self.add_file_to_db(db_found_letter,  self.path_leaf(f.name), filename)

    def save_file_to_folder(self, path_to_pdf, db_found_letter):
        self.__check_or_create_folder()

        full_file_name = path_to_pdf
        if (os.path.isfile(full_file_name)):
            shutil.copy(full_file_name,  self.full_path_file)
            self.add_file_to_db(db_found_letter, self.path_leaf(full_file_name), PDF_FILE_NAME)

        self.__delete_file_by_path(path_to_pdf)

    def __check_or_create_folder(self):
        try:
            os.mkdir(self.path_file)
            os.mkdir(self.full_path_file)
        except:
            pass

    def __delete_file_by_path(self, path):
        try:
            os.remove(path)
        except BaseException as e:
            pass

    def add_file_to_db(self, db_found_letter, file_name, origin_file_name):
        url = '/' + str(self.user_id) + '/' + file_name
        Files.objects.create(found_letter=db_found_letter,
                             name=origin_file_name,
                             url=url)

    def path_leaf(self, path):
        head, tail = ntpath.split(path)
        return tail or ntpath.basename(head)