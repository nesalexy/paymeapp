import datetime
from tempfile import NamedTemporaryFile

from emails.models import Emails
from emails_files.file_worker import File_Worker
from emails_settings.models import SendersSettings, EmailsSettings
from imap_gmail.constans import GETTING_LAST_MAILS_HOUR, GETTING_LAST_MAILS_MIN
from imap_gmail.imap_mail_helper import get_subject_from_mail, get_mail_from, get_date_for_mail, get_mail_id, get_mail, \
    string_to_date
from imap_gmail.pdf.PdfBox import PdfBox


class ImapWorker:

    def __init__(self, imap_connect, user, already_processed_mails_id, senders_search):
        self.imap_connect = imap_connect
        self.user = user
        self.file_worker = File_Worker(user_id=self.user.id)
        self.already_processed_mails_id = already_processed_mails_id
        self.senders_search = senders_search

    def check_mail_list(self):
        print("check_mail_list")
        self.imap_connect.select('INBOX', readonly=True)
        status, data = self.imap_connect.search(None, "ALL")
        if status == 'OK':
            items = data[0].split()
            for email_id in reversed(items):
                status, response = self.imap_connect.fetch(email_id, "(RFC822)")
                if status == 'OK':
                    mail = get_mail(response)
                    mail_id = get_mail_id(mail)
                    mail_date = get_date_for_mail(mail)

                    compare_date_mail = string_to_date(mail_date)
                    compare_date_now = datetime.datetime.now() - datetime.timedelta(hours=GETTING_LAST_MAILS_HOUR, minutes=GETTING_LAST_MAILS_MIN)
                    print("check mail date")

                    if compare_date_now <= compare_date_mail:

                        if not self.is_already_save(mail_id):
                            mail_from = get_mail_from(mail)

                            if self.check_sender(mail_from):
                                # generation pdf from email
                                mail_theme = get_subject_from_mail(mail)
                                db_found_letter = self.create_found_letter(
                                    mail_date,
                                    mail_id,
                                    mail_theme,
                                    mail_from
                                )

                                self.save_pdf(mail, db_found_letter)
                                self.save_attachment(mail, db_found_letter)

                                print('----')
                    else:
                        return

    def update_last_check_date(self):
        print("update_last_check_date")

        for email in self.senders_search:
            # update last date check current email
            settings_mail = EmailsSettings.objects.get(user=self.user)
            settings_sender = SendersSettings.objects.get(settings_mail=settings_mail, email=email)
            settings_sender.last_check_mail = datetime.datetime.now()
            settings_sender.save()

    # save pdf file
    def save_pdf(self, mail, db_found_letter):
        pdf_box = PdfBox(mail)
        with NamedTemporaryFile(delete=True, suffix=".html") as tf:
            path_to_pdf = pdf_box.generation_mail_pdf(tmp_html_file=tf.name)
            self.file_worker.save_file_to_folder(path_to_pdf, db_found_letter)

    # save attachments
    def save_attachment(self, mail, db_found_letter):
        for part in mail.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if part.get('Content-Disposition') is None:
                continue

            self.file_worker.save_payload_to_folder(part.get_filename(), part.get_payload(decode=True), db_found_letter)

    def check_sender(self, mail_from):
        return mail_from in self.senders_search

    def is_already_save(self, mail_id):
        return mail_id in self.already_processed_mails_id

    def upload_attachment(self, part):
        if part.get_content_maintype() == 'multipart':
            return

        if part.get('Content-Disposition') is None:
            return

    def create_found_letter(self, mail_date, mail_id, mail_theme, mail_from):
        return Emails.objects.create(user=self.user,
                                     mail_date=mail_date,
                                     mail_id=mail_id,
                                     mail_theme=mail_theme,
                                     mail_from=mail_from)