
def generateOAuth2String(user_email, access_token, base64_encode=False):
    """Generates an IMAP OAuth2 authentication string.

    See https://developers.google.com/google-apps/imap_gmail/oauth2_overview

    Args:
      username: the username (email address) of the account to authenticate
      access_token: An OAuth2 access token.
      base64_encode: Whether to base64-encode the output.

    Returns:
      The SASL argument for the OAuth2 mechanism.
    """
    auth_string = 'user=%s\1auth=Bearer %s\1\1' % (user_email, access_token)
    data_bytes = None
    # if base64_encode:
    #     data_bytes = auth_string.encode("utf-8")
    #     # auth_string = base64.b64encode(data_bytes)
    #     return data_bytes

    return auth_string.encode("utf-8")

