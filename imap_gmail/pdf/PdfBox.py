from tempfile import NamedTemporaryFile
from bs4 import BeautifulSoup
import re
import pdfkit

from imap_gmail.imap_mail_helper import get_mail_from, get_subject_from_mail, get_date_for_mail, get_mails_list

ADDR_PATTERN = re.compile('<(.*?)>')


# GENERATION HTML -> TO PDF , RETURN PATH TO PDF
class PdfBox:
    def __init__(self, mail):
        self.mail = mail

    def get_html_content(self, type):
        html_content = ''
        if type is not 'multipart/mixed':
            for part in self.mail.walk():
                if part.get_content_type() == type:
                    try:
                        soup = BeautifulSoup(
                            part.get_payload(decode=True).decode(errors='ignore').encode("ascii", errors="ignore"),
                            "lxml")

                        if soup.body is not None:
                            html_content += ''.join(['%s' % x for x in soup.body.contents])
                            # remove all img and src tags
                            html_content = self.remove_img_tags(html_content)
                            html_content = self.remove_font_src(html_content)
                            html_content = self.remove_url(html_content)

                    except BaseException as e:
                        print(e)
                        html_content = " "
                        break
        else:
            html_content = " "

        return html_content


    def create_pdf_file(self, tmp_html_file, type):
        is_created = False
        path_to_pdf = None

        try:
            html_content = self.get_html_content(type)

            if html_content:
                is_created = True
                self.gen_html_obj = GenerationHtml(mail=self.mail)

                try:
                    html_content = html_content.encode("ascii", errors="ignore").decode()
                except BaseException as e:
                    pass

                HtmlFile = open(tmp_html_file, "w")
                try:
                    HtmlFile.write(self.gen_html_obj.get_header())
                    HtmlFile.write(self.gen_html_obj.get_html_style())
                    HtmlFile.write(self.gen_html_obj.get_html_start_body())
                    try:
                        HtmlFile.write(self.gen_html_obj.get_html_header_info())
                    except BaseException as e:
                        print(e)
                    try:
                        HtmlFile.write(self.gen_html_obj.get_html_gray_line())
                    except BaseException as e:
                        print(e)

                    try:
                        HtmlFile.write(html_content)
                        HtmlFile.write(self.gen_html_obj.get_html_end())
                    except BaseException as e:
                        print(e)

                except BaseException as e:
                    print(e)

                finally:
                    HtmlFile.close()

                try:
                    with NamedTemporaryFile(delete=False, suffix=".pdf") as tf:
                        path_to_pdf = tf.name
                        pdfkit.from_file(tmp_html_file, tf.name)

                except BaseException as error:
                    print(error)

        except BaseException as e:
            print(e)

        return is_created, path_to_pdf

    def generation_mail_pdf(self, tmp_html_file):

        try:
            is_created, path_to_pdf = self.create_pdf_file(tmp_html_file, "text/html")
            if not is_created:
                is_created, path_to_pdf = self.create_pdf_file(tmp_html_file, "text/plain")
            if not is_created:
                is_created, path_to_pdf = self.create_pdf_file(tmp_html_file, "multipart/mixed")

        except BaseException as e:
            path_to_pdf = None

        return path_to_pdf

    def remove_img_tags(self, data):
        p = re.compile(r'<img(\n|.)*?/>')
        return p.sub('', data)

    def remove_font_src(self, data):
        p = re.compile(r'src:(\n|.)*?;')
        return p.sub('', data)

    def remove_url(self, data):
        p = re.compile(r'url\((\n|.)*?\)')
        return p.sub('', data)


class GenerationHtml:

    def __init__(self, mail):
        self.mail = mail

        mail_from = get_mail_from(mail)
        self.name_mail_from = self.get_name_mail_from(mail_from)
        self.email_mail_from = self.get_email_mail_from(mail_from)

        self.mail_subject = get_subject_from_mail(mail)
        self.mail_date = get_date_for_mail(mail["Date"])
        self.mail_to = get_mails_list(mail["To"])
        self.mail_cc = get_mails_list(mail["CC"])
        self.mail_bcc =get_mails_list(mail["Bcc"])

    def get_name_mail_from(self, mail_from):
        if mail_from is not None:
            return re.search(r'[\w\.-]+', mail_from).group(0)
        else:
            return ''

    def get_email_mail_from(self, mail_from):
        if mail_from is not None:
            return re.search(r'[\w\.-]+@[\w\.-]+', mail_from).group(0)
        else:
            return ''

    def get_header(self):
        return "<!DOCTYPE html> " \
               "<html>" \
               "<head> " \
               "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">" \
               "</head>"

    def get_html_style(self):
        return "<style> " \
               "p { display: inline; }" \
               "</style>"

    def get_html_start_body(self):
        return "<body text=\"#000000\" bgcolor=\"#FFFFFF\">"

    def get_html_header_info(self):
        try:
            return "<table style=\"width:100%\"> " \
               "<tr> " \
               "<td style=\"width:2%\" align=\"right\"><p><font size=\"4\" color=\"#ADA49F\">From: </font></p></td>" \
               "<td><p><b>&nbsp;&nbsp;" + self.name_mail_from + "</b> <font size=\"2.5\" color=\"#b2bec3\">" + \
                    self.email_mail_from + "</font></p></td></tr>" \
                 "<tr><td style=\"width:2%\" align=\"right\"><p><font size=\"4\" color=\"#ADA49F\">Subject:</font></p></td>" \
                 "<td><p>&nbsp;&nbsp;" + self.mail_subject + "</p></td></tr>" \
                                                             "<tr><td style=\"width:2%\" align=\"right\"><p><font size=\"4\" color=\"#ADA49F\">Date:</font></p></td>" \
                                                             "<td><p>&nbsp;&nbsp;" + self.mail_date + "</p></td></tr>" \
                                                                                                      "<tr><td style=\"width:2%\" align=\"right\"><p><font size=\"4\" color=\"#ADA49F\">To:</font></p></td>" \
                                                                                                      "<td><p>&nbsp;&nbsp;" + self.mail_to + "</p></td></tr>" \
                                                                                                                                             "<tr><td style=\"width:2%\" align=\"right\"><p><font size=\"4\" color=\"#ADA49F\">Cc:</font></p></td>" \
                                                                                                                                             "<td><p>&nbsp;&nbsp;" + self.mail_cc + "</p></td></tr>" \
                                                                                                                                                                                    "<tr><td style=\"width:2%\" align=\"right\"><p><font size=\"4\" color=\"#ADA49F\">Bcc:</font></p></td>" \
                                                                                                                                                                                    "<td><p>&nbsp;&nbsp;" + self.mail_bcc + "</p></td></tr></table>"
        except BaseException as e:
            print(e)
            return ' '

    def get_html_gray_line(self):
        try:
            return "<br><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important;\"><tr>" \
                   "<td align=\"left\" valign=\"top\" width=\"600px\" height=\"1\" style=\"background-color: #f0f0f0; " \
                   "border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; mso-line-height-rule: " \
                   "exactly; line-height: 1px;\"></td></tr></table><br>"
        except BaseException as e:
            return ' '

    def get_html_end(self):
        return "</body></html>"

