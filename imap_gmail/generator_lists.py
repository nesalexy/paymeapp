
from accounts.models import SocialAccounts
from emails_settings.models import SendersSettings, EmailsSettings
from emails.models import Emails


class GenerationSimpleData:

    def __init__(self, user):
        self.user = user

    def get_senders_search_dict(self):
        senders_search = {}
        senders_settings = None

        try:
            try:
                settings_mail = EmailsSettings.objects.get(user=self.user)
            except EmailsSettings.DoesNotExist:
                settings_mail = None
                senders_settings = None

            if settings_mail:
                senders_settings = SendersSettings.objects.filter(settings_mail=settings_mail)

        except SendersSettings.DoesNotExist:
            senders_settings = None

        if senders_settings:
            for item in senders_settings:
                senders_search[item.email] = item.last_check_mail

        return senders_search

    def get_already_processed_mails_id(self):
        already_processed_mails_id = []

        try:
            processed_mails = Emails.objects.filter(user=self.user)
        except Emails.DoesNotExist:
            processed_mails = None

        if processed_mails:
            for item in processed_mails:
                already_processed_mails_id.append(item.mail_id)

        return already_processed_mails_id

    def get_user_tokens(self):
        access_token = None
        refresh_token = None

        try:
            soc_accaunt = SocialAccounts.objects.get(user=self.user)
        except SocialAccounts.DoesNotExist:
            soc_accaunt = None

        if soc_accaunt:
            access_token = soc_accaunt.access_token
            refresh_token = soc_accaunt.refresh_token

        return access_token, refresh_token
