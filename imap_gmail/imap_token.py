import json

import httplib2

from imap_gmail.constans import CLIENT_ID, CLIENT_SECRET
from oauth2client import GOOGLE_TOKEN_URI, client, GOOGLE_REVOKE_URI
from accounts.models import SocialAccounts


class RefreshToken:

    def __init__(self, user, user_refresh_token):
        self.user = user
        self.user_refresh_token = user_refresh_token

    def refresh_token(self):

        credentials = client.OAuth2Credentials(
            access_token=None,  # set access_token to None since we use a refresh token
            client_id=CLIENT_ID,
            client_secret=CLIENT_SECRET,
            refresh_token=self.user_refresh_token,
            token_expiry=None,
            token_uri=GOOGLE_TOKEN_URI,
            user_agent=None,
            revoke_uri=GOOGLE_REVOKE_URI)

        credentials.refresh(httplib2.Http())  # refresh the access token (optional)
        data = json.loads(credentials.to_json())
        print(credentials.to_json())

        refresh_token = data["refresh_token"]
        access_token = data["access_token"]

        if refresh_token and access_token:
            return self.update_user_token(refresh_token, access_token)

        return False

    def update_user_token(self, refresh_token, access_token):
        try:
            soc_accaunt = SocialAccounts.objects.get(user=self.user)
        except SocialAccounts.DoesNotExist:
            soc_accaunt = None

        if soc_accaunt:
            soc_accaunt.access_token = access_token
            soc_accaunt.refresh_token = refresh_token
            soc_accaunt.save()

            return True

        return False
