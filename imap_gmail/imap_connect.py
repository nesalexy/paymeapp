import imaplib

from imap_gmail.constans import IMAP_PORT


class ImapMailConnect:

    def __init__(self, auth_string):
        self.auth_string = auth_string
        self.imap = None
        self.is_authenticate = False
        self.imap = imaplib.IMAP4_SSL(IMAP_PORT)
        self.imap.debug = 1

    def __enter__(self):
        if self.imap:
            try:
                self.imap.authenticate('XOAUTH2', lambda x: self.auth_string)
                self.is_authenticate = True
            except BaseException as e:
                self.is_authenticate = False

        return self.is_authenticate

    def __exit__(self):
        if self.imap:
            self.imap.close()
            self.imap.logout()