import datetime
import email
import re

ADDR_PATTERN = re.compile('<(.*?)>')

def get_mail(response):
    try:
        email_body = response[0][1].decode('utf8', 'ignore').encode("ascii", errors="ignore")
        mail = email.message_from_bytes(email_body)
        return mail
    except BaseException as e:
        email_body = response[0][1]
        mail = email.message_from_bytes(email_body)
        return mail


def get_subject_from_mail(mail):
    subject = ""
    try:
        subject = decode_mime_words(mail["Subject"].encode("ascii", errors="ignore").decode())
    except BaseException as e:
        print(e)

    return subject


def decode_mime_words(s):
    return u''.join(
        word.decode(encoding or 'utf8') if isinstance(word, bytes) else word
        for word, encoding in email.header.decode_header(s))


def get_mail_from(mail):
    mail_f = ""
    try:
        mail_from = re.findall(ADDR_PATTERN, mail['From'])
        if len(mail_from) == 0:
            mail_from.append(mail['From'])

        mail_f = decode_mime_words(mail_from[0].encode("ascii", errors="ignore").decode())
    except BaseException as e:
        print(e)

    return mail_f


def get_date_for_mail(mail):
    # October 24, 2017 at 12:49 PM EEST
    convert_date = ''
    try:
        date = mail["Date"]
        date_tuple = email.utils.parsedate_tz(date)
        date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
        convert_date = (datetime.datetime.strptime(str(date),
                                           '%Y-%m-%d %H:%M:%S')
                .strftime('%Y-%m-%d %H:%M:%S.%f'))
    except BaseException as e:
        pass

    return convert_date

def string_to_date(str_date):
    return datetime.datetime.strptime(str_date, '%Y-%m-%d %H:%M:%S.%f')



def get_mail_id(mail):
    return mail['Message-Id']


def get_mails_list(get_mails):
    mails = ""
    try:
        if get_mails is not None:
            mail_to_list = re.findall(ADDR_PATTERN, get_mails.encode("ascii", errors="ignore").decode())
            if len(mail_to_list) > 0:
                for mail in mail_to_list:
                    mails += mail + " "
            else:
                mails = get_mails.encode("ascii", errors="ignore").decode()

            return mails

    except BaseException as e:
        pass
    return mails
