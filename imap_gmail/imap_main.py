from imap_gmail.generator_lists import GenerationSimpleData
from imap_gmail.imap_connect import ImapMailConnect

from imap_gmail.imap_helper import generateOAuth2String
from imap_gmail.imap_token import RefreshToken
from imap_gmail.imap_worker import ImapWorker


class ImapMain:

    def __init__(self, user, senders_search):
        self.user = user
        self.gen_data = GenerationSimpleData(self.user)
        self.imap_connect = None
        self.senders_search = None
        self.already_processed_mails_id = None
        self.imap_worker = None
        self.senders_search = senders_search

    def start_processing(self):
        access_token, refresh_token = self.gen_data.get_user_tokens()
        print(str(access_token))
        print(str(refresh_token))

        if access_token and refresh_token:
            auth_string = generateOAuth2String(self.user.email, access_token, base64_encode=False)
            self.imap_connect = ImapMailConnect(auth_string)
            self.is_authenticate = self.imap_connect.__enter__()

            # update access token
            if not self.is_authenticate:
                print("update token")
                self.imap_connect, self.is_authenticate = self.refresh_user_token(refresh_token)

            if self.is_authenticate:
                already_processed_mails_id = self.gen_data.get_already_processed_mails_id()
                print("is_authenticate")

                if self.senders_search:
                    self.imap_worker = ImapWorker(self.imap_connect.imap,
                                                  self.user,
                                                  already_processed_mails_id,
                                                  self.senders_search)
                    self.imap_worker.check_mail_list()
                    self.imap_worker.update_last_check_date()

                self.imap_connect.__exit__()

            else:
                print('Cant authenticate')

        print("has not tokens")

    def refresh_user_token(self, refresh_token):
        gmail_refresh_token = RefreshToken(self.user, refresh_token)

        if gmail_refresh_token.refresh_token():
            access_token, refresh_token = self.gen_data.get_user_tokens()
            auth_string = generateOAuth2String(self.user.email, access_token, base64_encode=False)
            imap_connect = ImapMailConnect(auth_string)
            is_authenticate = imap_connect.__enter__()

            return imap_connect, is_authenticate


