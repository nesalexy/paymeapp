from rest_framework import generics, permissions, exceptions

from emails_settings.models import EmailsSettings, SendersSettings
from emails_settings.serializers import SettingsMailSerializer, EmailSettingsSerializer


class SettingsDetails(generics.RetrieveAPIView):
    """
    Settings for email.
    `GET` - return all settings for email
    """
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = SettingsMailSerializer
    queryset = EmailsSettings.objects.all()

    def get_object(self):
        try:
            return EmailsSettings.objects.get(user=self.request.user)

        except EmailsSettings.DoesNotExist:
            msg = 'Settings not found, please add senders'
            raise exceptions.NotFound(msg)


class SendersList(generics.ListCreateAPIView):
    """
    Senders that will be searched
    `GET` - return all senders
    `POST` - create senders item(`email` - required, `display_name` - not required)
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = EmailSettingsSerializer
    # queryset = SendersSettings.objects.all()

    def get_queryset(self, *args, **kwargs):
        try:
            settings_mail = EmailsSettings.objects.get(user=self.request.user)
            return SendersSettings.objects.all().filter(settings_mail=settings_mail)
        except:
            msg = 'Not found'
            raise exceptions.NotFound(msg)


class SendersDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Senders detail. View/Delete/Update sender item.
    `{id}` - id from senders list.
    `GET` - view item
    `PUT` - edit senders item(`email` - required, `display_name` - not required)
    `DELETE` - delete sender item
    """
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = EmailSettingsSerializer
    queryset = SendersSettings.objects.all()

    def get_object(self):
        try:
            settings_mail = EmailsSettings.objects.get(user=self.request.user)
            return SendersSettings.objects.all().filter(settings_mail=settings_mail, pk=self.kwargs['pk'])

        except EmailsSettings.DoesNotExist:
            msg = 'Not found'
            raise exceptions.NotFound(msg)