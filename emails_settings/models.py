from django.db import models
from django.contrib.auth.models import User


class EmailsSettings(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_settings')
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_at = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        verbose_name = 'Mail emails_settings'
        verbose_name_plural = 'Mails emails_settings'


class SendersSettings(models.Model):
    settings_mail = models.ForeignKey('EmailsSettings', on_delete=models.CASCADE, related_name='senders')
    email = models.EmailField(max_length=254, blank=False)
    display_name = models.CharField(max_length=254, blank=True)
    last_check_mail = models.DateTimeField(auto_now_add=False, auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_at = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        verbose_name = 'Email emails_settings'
        verbose_name_plural = 'Emails emails_settings'