from django.apps import AppConfig


class EmailsSettingsConfig(AppConfig):
    name = 'emails_settings'
