from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework import exceptions
from rest_framework.fields import SerializerMethodField

from emails_settings.models import EmailsSettings, SendersSettings


# !!!!!
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id',
                  'email',
                  'first_name',
                  'last_name')


class EmailSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = SendersSettings
        fields = ('id',
                  'settings_mail',
                  'email',
                  'display_name',
                  'last_check_mail',
                  )

        read_only_fields = ('id',
                            'settings_mail',
                            'last_check_mail',
                            )

    # check already exist sender email in current settings user
    def validate(self, attrs):
        email = attrs['email']
        current_user = self.context['request'].user
        settings = self.get_current_settings(current_user)

        try:
            senders_settings = SendersSettings.objects.get(settings_mail=settings, email=email)
        except SendersSettings.DoesNotExist:
            senders_settings = None

        if senders_settings:
            msg = 'Email already exist'
            raise exceptions.ParseError(msg)

        return attrs

    def create(self, validated_data):
        display_name = validated_data['display_name']
        email = validated_data['email']
        current_user = self.context['request'].user
        settings = self.get_current_settings(current_user)

        SendersSettings.objects.create(settings_mail=settings,
                                       email=email,
                                       display_name=display_name)

        return validated_data

    def get_current_settings(self, current_user):
        try:
            settings = EmailsSettings.objects.get(user=current_user)
        except EmailsSettings.DoesNotExist:
            settings = EmailsSettings.objects.create(user=current_user)

        return settings


class SettingsMailSerializer(serializers.ModelSerializer):
    senders = EmailSettingsSerializer(many=True, read_only=True)
    user = UserSerializer(read_only=True)

    class Meta:
        model = EmailsSettings
        fields = ('id', 'user', 'senders',)
        read_only_fields = ('id', 'user', 'senders',)
