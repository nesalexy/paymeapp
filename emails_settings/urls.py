from django.urls import path, re_path

from emails_settings import views

urlpatterns = [
    path('', views.SettingsDetails.as_view()),
    path('senders/', views.SendersList.as_view()),
    re_path('senders/(?P<pk>[0-9A-Za-z_\-]+)/', views.SendersDetail.as_view()),

]