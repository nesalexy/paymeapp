#### PaymeApp
 Developed with python 3.6

#### Run without Docker(Local)
- Run project
- Open terminal 1, run redis ``redis-server``
- Open terminal 2, run celery ``celery worker -A PaymeApp --loglevel=DEBUG --concurrency=4``

#### Run with Docker
 - ``docker-compose -f docker-compose.dev.yml build``
 - ``docker-compose -f docker-compose.dev.yml -p devapi up``