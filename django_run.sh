#!/usr/bin/env bash
set -e

if [ "$DJANGO_TEST" == "true" ]; then
    python manage.py test --noinput
fi
if [ "$COLLECT_STATIC" == "true" ]; then
    python manage.py collectstatic --noinput
fi

/usr/local/bin/gunicorn ${DJANGO_APP}.wsgi:application --timeout ${GUNICORN_TIMEOUT} --keep-alive ${GUNICORN_KKEP_ALIVE} -k gevent -w ${GUNICORN_WORKERS} --threads ${GUNICORN_THREADS} -b :${GUNICORN_PORT}

