from __future__ import absolute_import, unicode_literals

import os
from datetime import timedelta

from celery import Celery
import logging


logger = logging.getLogger("Celery")
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'PaymeApp.settings')

app = Celery('PaymeApp')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

# periodic task
app.conf.beat_schedule = {
    'send-report-every-single-minute': {
        'task': 'celery.pre_processing_mail',
        'schedule': timedelta(seconds=60),
    },
}


# @app.task(name='celery.send_mail')
# def send_mail():
#     from django.core.mail import EmailMessage
#     print("prepere_process_gmail")
#     user_email = "nesalexy@gmail.com"
#     mail_subject = "prepere_process_gmail"
#     to_email = user_email
#
#     email = EmailMessage(mail_subject, '', to=[to_email])
#     email.send()

@app.task
def start_process_gmail(user_id, senders_search_dict):
    from imap_gmail.imap_main import ImapMain
    from django.contrib.auth.models import User
    print("start_process_gmail")

    try:
        user = User.objects.get(pk=user_id)
        imap_worker = ImapMain(user, senders_search_dict)
        imap_worker.start_processing()
    except User.DoesNotExist:
        pass


@app.task(name='celery.pre_processing_mail')
def pre_processing_mail():
    from django.contrib.auth import get_user_model

    print("prepere_process_gmail")

    for user in get_user_model().objects.all():
        senders_search = get_senders_search(user)

        if senders_search:
            print("user id " + str(user.id))
            start_process_gmail.delay(user.id, senders_search)


def get_senders_search(user):
    print("get_senders_search")
    from emails_settings.models import EmailsSettings, SendersSettings

    senders_search = []
    senders_settings = None

    try:
        try:
            settings_mail = EmailsSettings.objects.get(user=user)
        except EmailsSettings.DoesNotExist:
            settings_mail = None
            senders_settings = None

        if settings_mail:
            senders_settings = SendersSettings.objects.filter(settings_mail=settings_mail)

    except SendersSettings.DoesNotExist:
        senders_settings = None

    if senders_settings:
        for item in senders_settings:
            sender_last_check = item.last_check_mail

            from django.utils.timezone import now, timedelta
            from imap_gmail.constans import MAIL_UPDATE_RATE_HOUR, MAIL_UPDATE_RATE_MIN
            now = now() - timedelta(hours=MAIL_UPDATE_RATE_HOUR, minutes=MAIL_UPDATE_RATE_MIN)

            if now >= sender_last_check:
                print("now " + str(now))
                print("sender_last_check " + str(sender_last_check))

                senders_search.append(item.email)

    return senders_search
